import React from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import './App.css';
import { Modules } from './component/Modules';
import { Header } from './component/Header';

function App() {
  return (
    <div className="App" style={{ height: '100vh' }}>
      <Header />
      <Modules />
    </div>
  );
}

export default App;
