import React, { useState } from 'react';
import { Home } from './modules/Home'
import { Profile } from './modules/Profile'

export const Modules = () => {
      const [content, setContent] = useState(<Home />);
      const [active, setActive] = useState("home");

      return (
        <div className="d-flex text-start bd-highlight h-100">
          <div className="p-2 w-15 bd-highlight border">
            <div className="d-flex align-items-start">
              <div className="nav flex-column nav-pills me-3" id="v-pills-tab" role="tablist" aria-orientation="vertical">
                <button
                  className={`nav-link ${active === "home" && "active"}`}
                  id="v-pills-home-tab"
                  type="button"
                  onClick={() => {
                      setActive("home")
                      setContent(<Home />)
                  }}
                  >
                    Home
                </button>
                <button
                  className={`nav-link ${active === "profile" && "active"}`}
                  id="v-pills-profile-tab"
                  onClick={() => {
                      setActive("profile")
                      setContent(<Profile />)
                  }}>
                    Profile
                  </button>
              </div>
            </div>
          </div>
          <div className="flex-grow-1 p-2 bd-highlight">
              {content}
          </div>
        </div>
      )
}