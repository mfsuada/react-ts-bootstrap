import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { solid } from '@fortawesome/fontawesome-svg-core/import.macro'

interface props {
    name : string,
    label : string,
    width? : string,
    additionalClass? : string
}

export const Input = (props : props) => {
    return (
      <div className={`d-flex w-${props.width ? props.width : '50'} mx-1 border border-gray rounded align-items-center p-1 g-1 input ${props.additionalClass}`}>
        <label className="bold opacity-50 mr-2">{props.label} : </label>
        <div className="flex-grow-1">
          <input className="w-100 border-0" name={props.name} />
        </div>
        <span className="btn btn-info btn-sm text-white mx-1"><FontAwesomeIcon icon={solid('clone')} /></span>
        <span className="btn btn-danger btn-sm mx-1"><FontAwesomeIcon icon={solid('trash')} /></span>
      </div>
    )
}