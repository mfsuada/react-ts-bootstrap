import { Input } from '../component/Input'
import { Textarea } from '../component/Textarea'

export const Profile = () => {
    return (
      <form>
        <div className="text-center mb-5">
          <h1>Form with class of flex</h1>
        </div>
        <div className="d-flex">
          <Input name="name" label="Name" />
          <Input name="identity_number" label="Identity number" />
        </div>
        <div className="d-flex">
          <Textarea name="current_address" width="100" additionalClass="mt-1" label="Current Address" />
        </div>

        <div className="d-flex text-justify">
          <p className="w-50 px-1 text-danger">
            Lorem ipsum dolor sit amet, consectetur adipiscing elit.
            Praesent pellentesque nec neque nec consectetur. Maecenas ut iaculis metus, fringilla eleifend mi.
            Cras id odio orci. Morbi nisi dui, fringilla et vulputate sodales, lobortis vitae quam.
            Cras aliquet consectetur quam a posuere.
            Pellentesque mi arcu, ultricies at congue et, dictum quis nulla.
            Aliquam malesuada erat et lorem fringilla, id semper nisi suscipit.
            Donec blandit, massa non imperdiet pulvinar, neque dolor consectetur elit,
            in rutrum sem enim eu tortor. Quisque sed sodales purus, vitae malesuada nisl.
            Aliquam vestibulum nec purus quis auctor. Nullam porttitor enim non ex eleifend, in viverra lacus ultricies.
            Nulla laoreet ligula arcu, sed condimentum ex lacinia vitae.
          </p>
          <p className="w-50 px-1 text-warning">
            Lorem ipsum dolor sit amet, consectetur adipiscing elit.
            Praesent pellentesque nec neque nec consectetur. Maecenas ut iaculis metus, fringilla eleifend mi.
            Cras id odio orci. Morbi nisi dui, fringilla et vulputate sodales, lobortis vitae quam.
            Cras aliquet consectetur quam a posuere.
            Pellentesque mi arcu, ultricies at congue et, dictum quis nulla.
            Aliquam malesuada erat et lorem fringilla, id semper nisi suscipit.
            Donec blandit, massa non imperdiet pulvinar, neque dolor consectetur elit,
            in rutrum sem enim eu tortor. Quisque sed sodales purus, vitae malesuada nisl.
            Aliquam vestibulum nec purus quis auctor. Nullam porttitor enim non ex eleifend, in viverra lacus ultricies.
            Nulla laoreet ligula arcu, sed condimentum ex lacinia vitae.
          </p>
        </div>
      </form>
    );
}
