import React from 'react';
import geekseat from "../assets/geekseat.svg"

export const Header = () => {
      return (
        <div className="d-flex text-start bd-highlight h-5 bg-primary">
          <div className="w-15 my-auto text-center">
              <img src={geekseat} width={180}/>
          </div>
          <div className="flex-grow-1 my-auto">

          </div>
        </div>
      )
}